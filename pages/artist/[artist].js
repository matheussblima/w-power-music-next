import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { Container, Footer, Header, ItemList, List } from "../../components";
import api from "../../config/api";
import { getArtist, clearArtist } from "../../duck/artist";
import { connect } from "react-redux";
import Link from "next/link";
import {
  Content,
  HeaderArtists,
  TitleViews,
  Title,
  SectionLyric,
  ListLyric,
  ListLyricTitle,
  ListLyricCount,
  SubTitle,
} from "../../assets/Styles/artist";

const RenderList = (artists) => {
  if (artists.artist) {
    return (
      <List
        data={artists.artist.artist.lyrics.item}
        renderItem={(item, index) => {
          return (
            <Link
              style={{ textDecoration: "none" }}
              href={`/lyric${item.url.replace(".html", "")}`}
            >
              <ListLyric>
                <ListLyricCount>{index + 1}</ListLyricCount>
                <ListLyricTitle>{item.desc}</ListLyricTitle>
              </ListLyric>
            </Link>
          );
        }}
      />
    );
  }
  return <div />;
};

const Artist = ({ artists }) => {
  return (
    <Container>
      <Header />
      {artists.artist ? (
        <>
          <Helmet>
            <title>{artists.artist.artist.desc} - wPowerMusic</title>
            <meta
              name="description"
              content={`Faça o download de powerpoint com as musicas de ${artists.artist.artist.desc}`}
            />
            <meta property="og:url" content={"wpowermusic.com"} />
            <meta property="og:type" content="article" />
            <meta
              property="article:section"
              content={`${artists.artist.artist.desc} - wPowerMusic`}
            />
            <meta property="og:site_name" content="wPowerMusic" />
            <meta property="og:locale" content={"pt-BR"} />
            <meta
              property="og:title"
              content={`${artists.artist.artist.desc} - wPowerMusic`}
            />
            <meta
              property="og:description"
              content={`Faça o download de powerpoint com as musicas de ${artists.artist.artist.desc}`}
            />
          </Helmet>
          <HeaderArtists>
            <ItemList
              disabled
              title={artists.artist.artist.desc}
              imageSrc={`${api.vagalume}${artists.artist.artist.pic_medium}`}
            />
            {artists.artist ? (
              <TitleViews>{`${artists.artist.artist.rank.views} exibições`}</TitleViews>
            ) : undefined}
          </HeaderArtists>
          <Content>
            <Title>Letras</Title>
            <SectionLyric>{RenderList(artists)}</SectionLyric>
          </Content>
        </>
      ) : undefined}
      <Content marginTop={"72px"}>
        {artists.isFetchArtist ? (
          <SubTitle>Carregando...</SubTitle>
        ) : artists.isErrorArtist ? (
          <SubTitle>{artists.message}</SubTitle>
        ) : undefined}
      </Content>
      <Footer />
    </Container>
  );
};

Artist.getInitialProps = ({ store, query }) => {
  return store.dispatch(getArtist(query.artist)).then((response) => {
    return { artists: { ...response, artist: response.payload } };
  });
};

const mapDispatchToProps = (dispatch) => ({
  clearArtist: () => dispatch(clearArtist()),
});

export default connect(null, mapDispatchToProps)(Artist);
