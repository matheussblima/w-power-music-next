import React, { useEffect, useState } from "react";
import Proptypes from "prop-types";
import { Helmet } from "react-helmet";
import AdSense from "react-adsense";
import {
  Container,
  Footer,
  Header,
  ItemList,
  Button,
  Modal,
} from "../../components";
import { connect } from "react-redux";
import api from "../../config/api";
import { getLyric } from "../../duck/lyric";
import { createSlide, downloadSlide } from "../../duck/slide";
import {
  Content,
  HeaderArtists,
  SectionLyric,
  LyricText,
  SubTitle,
  Title,
  SectionModal,
  SectionModalBottom,
} from "../../assets/Styles/lyric";
import { AdsContainer } from "../../assets/Styles/home";

const Lyric = ({ lyric, createSlide, downloadSlide, slide, query }) => {
  const [showModal, setShowModal] = useState(false);

  const onClickCreateSlide = () => {
    createSlide({
      artist: lyric.lyric.art.name,
      music: lyric.lyric.mus[0].name,
      lyric: lyric.lyric.mus[0].text,
    });
  };

  const onClickDownload = () => {
    downloadSlide(slide.slide.fileId);
    setShowModal(false);
  };

  useEffect(() => {
    if (slide.isSuccessSlide) {
      setShowModal(true);
    }
  }, [slide.isSuccessSlide]);

  return (
    <Container>
      <Modal showModal={showModal} onClickClose={() => setShowModal(false)}>
        <SectionModal>
          <Title textAlign="center">Download</Title>
          <SubTitle textAlign="center">
            Faça o download de seu slide no formato (pptx)
          </SubTitle>
          <SectionModalBottom>
            <Button icon="/images/download.png" onClick={onClickDownload}>
              Download
            </Button>
          </SectionModalBottom>
        </SectionModal>
      </Modal>
      <Header />
      {lyric.isSuccessLyric ? (
        <>
          <Helmet>
            <title>
              {lyric.lyric.mus[0].name} - {lyric.lyric.art.name} - wPowerMusic
            </title>
            <meta
              name="description"
              content={`Faça o download de powerpoint com a musica ${lyric.lyric.mus[0].name} de ${lyric.lyric.art.name}`}
            />
          </Helmet>
          <HeaderArtists>
            <ItemList
              disabled
              title={lyric.lyric.mus[0].name}
              subtitle={lyric.lyric.art.name}
              imageSrc={`${api.vagalume}/${query.artist}/images/${query.artist}.jpg`}
            />
          </HeaderArtists>
          <AdsContainer>
            <AdSense.Google
              client="ca-pub-7960141731149048"
              slot="5134572060"
              style={{
                display: "block",
                height: 90,
                width: 728,
                marginLeft: "auto",
                marginRight: "auto",
              }}
              format="auto"
              responsive="true"
            />
          </AdsContainer>
          <Content>
            <Button
              loading={slide.isFetchSlide}
              icon="/images/slideshow.png"
              onClick={onClickCreateSlide}
            >
              Gerar slide
            </Button>
            <SectionLyric>
              <LyricText>{lyric.lyric.mus[0].text}</LyricText>
            </SectionLyric>
          </Content>
        </>
      ) : undefined}
      <Content marginTop={"52px"} alignItems="Left">
        {lyric.isFetchLyric ? (
          <SubTitle>Carregando...</SubTitle>
        ) : lyric.isErrorLyric ? (
          <SubTitle>{lyric.message}</SubTitle>
        ) : undefined}
      </Content>
      <Footer />
    </Container>
  );
};

Lyric.propTypes = {
  lyric: Proptypes.object,
  getLyric: Proptypes.func,
  createSlide: Proptypes.func,
  downloadSlide: Proptypes.func,
  slide: Proptypes.object,
};

Lyric.getInitialProps = async ({ store, query }) => {
  const artist = query.all[0];
  const music = query.all[1];

  const getLyricResponse = await store.dispatch(getLyric(artist, music));

  return {
    lyric: {
      ...getLyricResponse,
      lyric: getLyricResponse.payload,
    },
    query: { artist, music },
  };
};

const mapStateToProps = (state) => ({
  slide: state.slide,
});

const mapDispatchToProps = (dispatch) => ({
  createSlide: (slide) => dispatch(createSlide(slide)),
  downloadSlide: (fileId) => dispatch(downloadSlide(fileId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Lyric);
