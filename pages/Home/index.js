import React, { useState } from "react";
import url from "url-parse";
import AdSense from "react-adsense";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Container,
  Header,
  ImageOverlayTitle,
  ItemList,
  Footer,
} from "../../components";
import { search } from "../../duck/search";
import { getRankArtist, getRankMusic } from "../../duck/rank";
import Skeleton from "../../assets/Skeleton/home";
import {
  SectionArtist,
  Content,
  Title,
  SubTitle,
  AdsContainer,
  SectionMusicas,
} from "../../assets/Styles/home";

var searchDelay = null;
const Home = ({ search, searchLyric, rank }) => {
  const [searchValue, setSearch] = useState("");
  const [loading, setLoading] = useState(false);

  const onSearch = (event) => {
    const inputValue = event.target.value;
    setSearch(inputValue);
    setLoading(true);

    clearTimeout(searchDelay);
    searchDelay = setTimeout(() => {
      searchLyric(inputValue);
      setLoading(false);
    }, 1000);
  };

  const data = {
    artists:
      search.isSuccessSearch && search.search.artist && searchValue
        ? search.search.artist.response.docs
        : rank.rankArtist && !searchValue
        ? rank.rankArtist.art.week.all
        : [],
    musics:
      search.isSuccessSearch && search.search.artmus && searchValue
        ? search.search.artmus.response.docs
        : rank.rankMusic && !searchValue
        ? rank.rankMusic.mus.week.all
        : [],
    excerpts:
      search.isSuccessSearch && search.search.artmus && searchValue
        ? search.search.excerpt.response.docs
        : [],
  };

  return (
    <Container>
      <Header search={true} value={searchValue} onChange={onSearch} />

      {rank.isFetchRankMusic || rank.isFetchRankArtist ? (
        <Content>
          <SubTitle>Carregando...</SubTitle>
        </Content>
      ) : rank.isErrorRank ? (
        <Content>
          <SubTitle>{rank.message}</SubTitle>
        </Content>
      ) : (
        <Content>
          {loading || search.isFetchSearch ? (
            <Skeleton />
          ) : search.isErrorSearch ? (
            <SubTitle>{search.message}</SubTitle>
          ) : undefined}

          {data.artists.length > 0 ? (
            <>
              <AdsContainer>
                <AdSense.Google
                  client="ca-pub-7960141731149048"
                  slot="5134572060"
                  style={{
                    display: "block",
                    height: 90,
                    width: 728,
                    marginLeft: "auto",
                    marginRight: "auto",
                  }}
                  format="auto"
                  responsive="true"
                />
              </AdsContainer>
              <Title>Artistas</Title>
              <SectionArtist>
                {!search.isFetchSearch &&
                  data.artists.map((artist, index) => {
                    return (
                      <ImageOverlayTitle
                        key={index}
                        imageSrc={artist.pic_medium}
                        href={`/artist/${artist.url.split("/").join("")}`}
                      >
                        {artist.name || artist.band}
                      </ImageOverlayTitle>
                    );
                  })}
              </SectionArtist>
            </>
          ) : undefined}

          {data.musics.length > 0 ? (
            <>
              <Title>Músicas</Title>
              <SectionMusicas>
                {data.musics.map((music, index) => (
                  <a
                    key={index}
                    style={{ textDecoration: "none" }}
                    href={
                      music.name || music.title
                        ? `/lyric${url(music.url, true).pathname.replace(
                            ".html",
                            ""
                          )}`
                        : `/artist${url(music.url, true).pathname.replace(
                            ".html",
                            ""
                          )}`
                    }
                  >
                    <ItemList
                      title={music.name || music.title}
                      subtitle={music.art ? music.art.name : music.band}
                      imageSrc={
                        music.art ? music.art.pic_medium : music.pic_medium
                      }
                    />
                  </a>
                ))}
              </SectionMusicas>
            </>
          ) : undefined}

          {data.excerpts.length > 0 ? (
            <>
              <Title>Trechos</Title>
              <SectionMusicas>
                {data.excerpts.map((excerpt, index) => (
                  <a
                    key={index}
                    style={{ textDecoration: "none" }}
                    href={
                      excerpt.title
                        ? `/lyric${url(excerpt.url).pathname.replace(
                            ".html",
                            ""
                          )}`
                        : `/artist${url(excerpt.url).pathname.replace(
                            ".html",
                            ""
                          )}`
                    }
                  >
                    <ItemList
                      key={index}
                      title={excerpt.title}
                      subtitle={excerpt.band}
                      imageSrc={excerpt.pic_medium}
                    />
                  </a>
                ))}
              </SectionMusicas>
            </>
          ) : undefined}
        </Content>
      )}
      <Footer />
    </Container>
  );
};

Home.propTypes = {
  searchLyric: PropTypes.func,
  search: PropTypes.object,
  rank: PropTypes.object,
  getRankArtist: PropTypes.func,
  getRankMusic: PropTypes.func,
};

Home.getInitialProps = async ({ store }) => {
  const getRankArtistResponse = await store.dispatch(getRankArtist());
  const getRankMusicResponse = await store.dispatch(getRankMusic());

  return {
    rank: {
      ...getRankArtistResponse,
      ...getRankMusicResponse,
      rankArtist: getRankArtistResponse.payload,
      rankMusic: getRankMusicResponse.payload,
    },
  };
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchLyric: (keywords) => dispatch(search(keywords)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
