import Document, { Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();

    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />)
    );

    const styleTags = sheet.getStyleElement();

    return { ...page, styleTags };
  }

  render() {
    return (
      <html>
        <Head>
          <title>wPowerMusic - Download de powerpoint de músicas</title>
          <meta
            name="title"
            content="wpowermusic - Gerar apresentações em powerpoint de suas músicas favoritas."
          />
          <meta
            name="description"
            content="Encontre letras de musicas e faça o download de um arquivo pptx (PowerPoint) com as mesmas."
          />
          <meta
            name="keywords"
            content="slide, powerpoint, apresentação, musicas, letras, download letras"
          />
          <meta http-equiv="Content-Language" content="pt-br" />
          {this.props.styleTags}
          <script
            data-ad-client="ca-pub-7960141731149048"
            async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          ></script>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
