import React from "react";
import { ThemeProvider } from "styled-components";
import { Provider } from "react-redux";
import App from "next/app";
import withRedux from "next-redux-wrapper";
import { Fonts } from "../assets";
import Theme from "../theme";
import configureStore from "../store";

import "../index.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

export default withRedux(configureStore, { debug: true })(
  class WPowerMusic extends App {
    render() {
      const { Component, pageProps, store } = this.props;
      return (
        <Provider store={store}>
          <Fonts />
          <ThemeProvider theme={Theme}>
            <Component {...pageProps} />
          </ThemeProvider>
        </Provider>
      );
    }
  }
);
