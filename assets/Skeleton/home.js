import React from "react";
import styled from "styled-components";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const Container = styled.div`
  padding: 16px;
`;

const Content = styled.div`
  margin-bottom: 16px;
  margin-right: 20px;
`;

const Section = styled.div`
  margin-top: 40px;
  display: flex;
  flex-wrap: wrap;
`;

export default () => (
  <Container>
    <SkeletonTheme color="#e4e1e1" highlightColor="#FFFF">
      <Content>
        <Skeleton width={400} height={50} />
      </Content>
      <Section>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
        <Content>
          <Skeleton width={220} height={120} />
        </Content>
      </Section>
    </SkeletonTheme>
  </Container>
);
