import styled from "styled-components";

export const Content = styled.section`
  display: flex;
  flex: 1;
  padding: 20px 12px;
  margin-top: ${(props) => props.marginTop || 0};
  flex-direction: column;
`;

export const HeaderArtists = styled.section`
  background-color: ${(props) => props.theme.fourthColor};
  max-width: 100%;
  min-height: 64px;
  display: flex;
  justify-content: space-between;
  margin-top: 72px;
  align-items: center;
  padding: 4px 8px;

  @media screen and (max-width: 600px) {
    margin-top: 60px;
  }
`;

export const TitleViews = styled.h3`
  color: ${(props) => props.theme.primaryColor};
  margin: 16px;
  font-family: Raleway;
`;

export const Title = styled.h1`
  font-family: Raleway;
  color: ${(props) => props.theme.primaryColor};
  margin: 8px 16px 0px 16px;
  font-size: 38px;
  font-weight: bold;

  @media screen and (max-width: 600px) {
    font-size: 30px;
  }
`;

export const SectionLyric = styled.section`
  display: flex;
  flex-direction: column;
  margin-bottom: 40px;
`;

export const ListLyric = styled.div`
  display: flex;
  align-items: center;
  text-decoration: none;
  cursor: pointer;
  padding: 6px;
  width: 100%;

  &:hover {
    background-color: rgba(0, 0, 0, 0.2);
    padding: 6px;
    border-radius: 8px;
  }

  &:active {
    background-color: rgba(0, 0, 0, 0.4);
  }
`;

export const ListLyricTitle = styled.h1`
  margin: 8px;
  color: ${(props) => props.theme.primaryColor};
  font-family: Raleway;
`;

export const ListLyricCount = styled.h1`
  width: 20px;
  margin: 0px;
  margin-right: 16px;
  font-size: 20px;
  color: ${(props) => props.theme.seventhColors};
  font-family: Raleway;
`;

export const SubTitle = styled.h2`
  font-family: Raleway;
  color: ${(props) => props.theme.primaryColor};
  margin: 8px 16px;
  font-size: 20px;
  font-weight: bold;

  @media screen and (max-width: 600px) {
    font-size: 16px;
  }
`;
