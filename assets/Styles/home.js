import styled from "styled-components";

export const Content = styled.section`
  display: flex;
  flex: 1;
  padding: 32px 12px;
  margin-top: 60px;
  flex-direction: column;
`;

export const SectionArtist = styled.section`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 40px;
`;

export const SectionMusicas = styled.section`
  display: flex;
  flex-direction: column;
  margin-bottom: 40px;
`;

export const Title = styled.h1`
  font-family: Raleway;
  color: ${(props) => props.theme.primaryColor};
  margin: 8px 16px;
  font-size: 38px;
  font-weight: bold;

  @media screen and (max-width: 600px) {
    font-size: 30px;
  }
`;

export const SubTitle = styled.h2`
  font-family: Raleway;
  color: ${(props) => props.theme.primaryColor};
  margin: 8px 16px;
  font-size: 20px;
  font-weight: bold;

  @media screen and (max-width: 600px) {
    font-size: 16px;
  }
`;

export const AdsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90px;
  margin-bottom: 16px;
  width: 100%;
`;
