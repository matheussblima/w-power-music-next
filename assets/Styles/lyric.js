import styled from "styled-components";

export const Content = styled.section`
  display: flex;
  flex: 1;
  align-items: ${props => props.alignItems || "center"};
  padding: 40px 12px 20px 12px;
  margin-top: ${props => props.marginTop || 0};
  flex-direction: column;
`;

export const HeaderArtists = styled.section`
  background-color: ${props => props.theme.fourthColor};
  max-width: 100%;
  min-height: 64px;
  display: flex;
  justify-content: space-between;
  margin-top: 72px;
  align-items: center;
  padding: 4px 8px;

  @media screen and (max-width: 600px) {
    margin-top: 60px;
  }
`;

export const TitleViews = styled.h3`
  color: ${props => props.theme.primaryColor};
  margin: 16px;
  font-family: Raleway;
`;

export const Title = styled.h1`
  font-family: Raleway;
  color: ${props => props.theme.primaryColor};
  margin: 8px 16px 0px 16px;
  font-size: 30px;
  font-weight: bold;
  text-align: ${props => props.textAlign || "left"};
  @media screen and (max-width: 600px) {
    font-size: 20px;
  }
`;

export const SectionLyric = styled.div`
  white-space: pre-line;
  padding: 20px;
`;

export const LyricText = styled.p`
  text-align: center;
  font-family: Raleway;
  font-size: 20px;

  @media screen and (max-width: 600px) {
    font-size: 16px;
  }
`;

export const SubTitle = styled.h2`
  font-family: Raleway;
  color: ${props => props.theme.primaryColor};
  margin: 8px 16px;
  font-size: 20px;
  font-weight: bold;
  text-align: ${props => props.textAlign || "left"};

  @media screen and (max-width: 600px) {
    font-size: 16px;
  }
`;

export const SectionModal = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const SectionModalBottom = styled.div`
  margin-top: 20px;
`;
