const Images = {
  search: require("./search.png"),
  close: require("./close.png"),
  slide: require("./slideshow.png"),
  download: require("./download.png")
};

export default Images;
