import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Content = styled.div`
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.4);
  background-color: ${props => props.backgroundColor || props.theme.thirdColor};
  display: flex;
  align-item: center;
  border-radius: 6px;
  flex: 1;
  min-width: 50vw;
`;

const InputComponent = styled.input`
  padding: 0px 16px 0px 16px;
  border-radius: 0px 6px 6px 0px;
  height: 46px;
  display: flex;
  flex: 1;
  border-width: 0px;
  border: none;
  font-family: Raleway;
  font-size: 16px;
  background-color: rgba(0, 0, 0, 0) !important;
  color: ${props => props.textColor || props.theme.fourthColor};
  width: 5px;
  &:focus {
    outline: none;
  }

  &::placeholder {
    color: ${props => props.textColorPlaceholder || props.theme.fifthColor};
  }

  @media screen and (max-width: 500px) {
    width: 50px;
    height: 40px;
  }
`;

const Icon = styled.img`
  width: 20px;
  height: 20px;
`;

const ContentIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0px 0px 0px 16px;
`;

class Input extends React.Component {
  render() {
    const { icon } = this.props;
    return (
      <Content>
        {icon ? (
          <ContentIcon>
            <Icon src={icon} alt="icon" {...this.props} />
          </ContentIcon>
        ) : (
          undefined
        )}
        <InputComponent {...this.props} />
      </Content>
    );
  }
}

Input.propTypes = {
  icon: PropTypes.string
};

export default Input;
