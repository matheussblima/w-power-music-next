import React from "react";
import styled from "styled-components";

const FooterComponent = styled.footer`
  background-color: ${props => props.theme.fourthColor};
  padding: 8px 26px;
  box-shadow: 2px 0px 4px rgba(0, 0, 0, 0.4);
  bottom: 0;
  position: relative;
`;

const Title = styled.h2`
  color: ${props => props.theme.sixthColor};
  font-size: 20px;
`;

const Footer = () => {
  return (
    <FooterComponent>
      <Title>© 2020 WpowerMusic. All rights reserved.</Title>
    </FooterComponent>
  );
};

export default Footer;
