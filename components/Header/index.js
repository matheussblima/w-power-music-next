import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Logo from "../Logo";
import Input from "../Input";

const HeaderComponent = styled.div`
  overflow: hidden;
  background-color: ${(props) =>
    props.backgroundColor || props.theme.primaryColor};
  padding: 22px 16px;
  max-height: 28px;
  display: flex;
  align-items: center;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.4);
  position: fixed;
  z-index: 1000;
  width: 100%;

  @media screen and (max-width: 600px) {
    max-height: 16px;
  }
`;

const Link = styled.a`
  text-decoration: none;
  padding: 6px 12px;

  &:hover {
    background-color: rgba(255, 255, 255, 0.2);
    padding: 6px 12px;
    border-radius: 8px;
  }

  &:active {
    background-color: rgba(255, 255, 255, 0.4);
  }
`;

const SectionInput = styled.div`
  margin: 0px 16px;

  @media screen and (max-width: 600px) {
    margin: 0px 4px;
  }
`;

const Header = (props) => {
  return (
    <HeaderComponent>
      <Link href={props.logoHref}>
        <Logo />
      </Link>
      {props.search ? (
        <SectionInput>
          <Input
            placeholder={props.placeholderSearch}
            icon="/images/search.png"
            value={props.value}
            onChange={props.onChange}
          />
        </SectionInput>
      ) : undefined}
    </HeaderComponent>
  );
};

Header.propTypes = {
  onPresslogo: PropTypes.func,
  placeholderSearch: PropTypes.string,
  logoHref: PropTypes.string,
  search: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

Header.defaultProps = {
  placeholderSearch: "Músicas, Artistas, Trechos",
  logoHref: "/",
  search: false,
};

export default Header;
