import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const ModalComponent = styled.div`
  position: absolute;
  background-color: rgba(0, 0, 0, 0.6);
  width: 100vw;
  height: 100vh;
  left: 0;
  top: 0;
  position: fixed;
  overflow: auto;
  z-index: 2000;
  display: ${(props) => (props.showModal ? "flex" : "none")};
  flex: 1;
  flex-direction: column;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.4);
`;

const Content = styled.div`
  width: 70%;
  background-color: ${(props) => props.theme.fourthColor};
  border-radius: 8px;
  min-height: 20px;
  padding: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Header = styled.div`
  display: flex;
  flex: 0;

  justify-content: flex-end;
  padding: 20px 30px;
`;

const Icon = styled.img`
  width: 30px;
  height: 30px;

  &:hover {
    opacity: 0.6;
  }

  &:active {
    opacity: 0.4;
  }
`;

const Section = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Modal = (props) => {
  return (
    <ModalComponent showModal={props.showModal}>
      <Header>
        <Icon src="/images/close.png" onClick={props.onClickClose} />
      </Header>
      <Section>
        <Content>{props.children}</Content>
      </Section>
    </ModalComponent>
  );
};

Modal.propTypes = {
  children: PropTypes.any,
  showModal: PropTypes.bool,
  onClickClose: PropTypes.func,
};

export default Modal;
