import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Loader from "react-loader-spinner";

const ButtonComponent = styled.button`
  border: none;
  background-color: ${(props) =>
    props.backgroundColor || props.theme.eighthColor};
  color: ${(props) => props.textColor || props.theme.fourthColor};
  font-family: Raleway;
  font-weight: bold;
  font-size: 16px;
  padding: 12px 28px;
  border-radius: 8px;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.4);
  align-items: center;
  justify-content: center;
  display: flex;
  min-width: 200px;
  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.6;
  }
`;

const Container = styled.div``;

const Image = styled.img`
  width: auto;
  height: 30px;
  margin-right: 8px;
`;

const Button = (props) => {
  return (
    <Container>
      {!props.loading ? (
        <ButtonComponent {...props}>
          <Image src={props.icon} alt="icon" />
          {props.children}
        </ButtonComponent>
      ) : (
        <ButtonComponent disabled {...props}>
          <Loader
            type="Puff"
            color={props.colorLoader || "#FFFFFF"}
            height={30}
            width={30}
          />
        </ButtonComponent>
      )}
    </Container>
  );
};

Button.propTypes = {
  children: PropTypes.any,
  icon: PropTypes.string,
};

export default Button;
