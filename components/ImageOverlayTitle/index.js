import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Container = styled.a`
  position: relative;
  width: 240px;
  height: 120px;
  overflow: hidden;
  border-radius: 8px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.4);
  background-color: ${props => props.theme.fifthColor};
  margin: 16px;

  &:hover {
    opacity: 0.8;
    cursor: pointer;
  }

  &:active {
    opacity: 0.6;
  }

  @media screen and (max-width: 600px) {
    height: 90px;
    width: 100%;
  }
`;

const Image = styled.img`
  width: 100%;
  height: auto;
`;

const Overlay = styled.div`
  position: absolute;
  background-color: rgba(0, 0, 0, 0.2);
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  bottom: 0;
  top: 0;
`;

const Title = styled.span`
  color: ${props => props.theme.fourthColor};
  font-size: 30px;
  font-family: Raleway;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  padding: 8px 20px;
  font-weight: bold;

  @media screen and (max-width: 600px) {
    font-size: 20px;
  }
`;

const ImageOverlayTitle = props => {
  return (
    <Container onClick={props.onClick} href={props.href}>
      {props.imageSrc ? (
        <Image src={props.imageSrc} alt={props.children || "artista"} />
      ) : (
        undefined
      )}

      <Overlay>
        <Title>{props.children}</Title>
      </Overlay>
    </Container>
  );
};

ImageOverlayTitle.propTypes = {
  title: PropTypes.string,
  imageSrc: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.string,
  href: PropTypes.string
};

export default ImageOverlayTitle;
