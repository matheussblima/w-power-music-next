import React from "react";
import styled from "styled-components";

const LogoComponent = styled.h1`
  color: ${props => props.color || "#FFFFFF"};
  font-size: ${props => props.size || "30px"};
  text-align: ${props => props.align || "left"};
  font-family: Raleway;

  @media screen and (max-width: 600px) {
    font-size: 18px;
  }
  margin: 0;
`;

class Logo extends React.Component {
  render() {
    return (
      <div>
        <LogoComponent>wPowerMusic</LogoComponent>
      </div>
    );
  }
}

export default Logo;
