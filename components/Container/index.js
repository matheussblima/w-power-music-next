import styled from "styled-components";

export default styled.div`
  display: flex;
  flex: 1;
  background-color: ${props =>
    props.backgroundColor || props.theme.secondColor};
  min-height: 100vh;
  flex-direction: column;
`;
