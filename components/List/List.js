import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Container = styled.div`
  margin: 16px;
`;

const List = props => {
  return (
    <Container>
      {props.data.map((item, index) => (
        <div key={index}>{props.renderItem(item, index)}</div>
      ))}
    </Container>
  );
};

List.propTypes = {
  data: PropTypes.array.isRequired,
  renderItem: PropTypes.func.isRequired
};

export default List;
