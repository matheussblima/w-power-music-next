import React from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin: 16px;
  align-items: center;

  ${props =>
    !props.disabled &&
    css`
      &:hover {
        opacity: 0.8;
        cursor: pointer;
      }

      &:active {
        opacity: 0.6;
      }
    `}

  @media screen and (max-width: 400px) {
    flex-direction: column;
  }
`;

const TitleSection = styled.div``;

const Title = styled.h1`
  margin: 0;
  font-size: 30px;
  color: ${props => props.textColor || props.theme.primaryColor};
  text-overflow: ellipsis;
  word-wrap: break-word;
  overflow: hidden;
  max-width: 600px;
  font-family: Raleway;
  font-weight: bold;

  @media screen and (max-width: 600px) {
    font-size: 20px;
    max-width: 300px;
  }

  @media screen and (max-width: 400px) {
    text-align: center;
  }
`;

const Subtitle = styled.h2`
  margin: 0;
  font-size: 20px;
  color: ${props => props.textColor || props.theme.primaryColor};
  font-family: Raleway;

  @media screen and (max-width: 600px) {
    font-size: 14px;
  }

  @media screen and (max-width: 400px) {
    text-align: center;
  }
`;

const ImageContainer = styled.div`
  background-color: ${props => props.theme.fifthColor};
  width: 80px;
  height: 80px;
  border-radius: 40px;
  margin-right: 16px;
  overflow: hidden;

  ${props =>
    !props.imageSrc &&
    css`
      display: flex;
      align-items: center;
      justify-content: center;
    `};

  @media screen and (max-width: 600px) {
    width: 60px;
    height: 60px;
    border-radius: 30px;
  }
`;

const Image = styled.img`
  width: 80px;
  object-fit: contain;
`;

const ItemList = props => {
  return (
    <Container disabled={props.disabled}>
      <ImageContainer>
        {!props.imageSrc ? (
          <Title>{props.subtitle.slice(0, 2)}</Title>
        ) : (
          <Image src={props.imageSrc} alt={props.title || "artista"} />
        )}
      </ImageContainer>
      <TitleSection>
        <Title>{props.title}</Title>
        <Subtitle>{props.subtitle}</Subtitle>
      </TitleSection>
    </Container>
  );
};

ItemList.propTypes = {
  imageSrc: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  disabled: PropTypes.bool
};

ItemList.defaultProps = {
  subtitle: ""
};

export default ItemList;
