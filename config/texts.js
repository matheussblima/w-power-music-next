export default lang => {
  const language = lang || navigator.language;
  const texts = {
    "pt-BR": {
      home: {
        artistTitle: "Artista"
      }
    },
    "en-US": {
      home: {
        artistTitle: "Artist"
      }
    }
  };
  return texts[language] || texts["pt-BR"];
};
