const theme = {
  primaryColor: "#333333",
  secondColor: "#F4F4F4",
  thirdColor: "#484848",
  fourthColor: "#FFFFFF",
  fifthColor: "#DDDDDD",
  sixthColor: "#999999",
  seventhColors: "#A5A3A3",
  eighthColor: "#FF4F4F"
};

export default theme;
