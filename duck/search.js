import axios from "axios";
import api from "../config/api";

const stauts = Object.freeze({
  success: "SUCCESS",
  failure: "FAILURE",
});

// Actions
const SEARCH_REQUEST = "wpowermusic/SEARCH_REQUEST";
const SEARCH_SUCCESS = "wpowermusic/SEARCH_SUCCESS";
const SEARCH_FAILURE = "wpowermusic/SEARCH_FAILURE";

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SEARCH_REQUEST:
      return Object.assign({}, state, {
        isFetchSearch: action.isFetchSearch,
        isSuccessSearch: action.isSuccessSearch,
        isErrorSearch: action.isErrorSearch,
        search: null,
      });
    case SEARCH_SUCCESS:
      return Object.assign({}, state, {
        isFetchSearch: action.isFetchSearch,
        isSuccessSearch: action.isSuccessSearch,
        statusSearch: action.statusSearch,
        search: action.payload,
        isErrorSearch: action.isErrorSearch,
      });
    case SEARCH_FAILURE:
      return Object.assign({}, state, {
        isFetchSearch: action.isFetchSearch,
        isSuccessSearch: action.isSuccessSearch,
        statusSearch: action.statusSearch,
        message: action.message,
        isErrorSearch: action.isErrorSearch,
      });
    default:
      return state;
  }
}

// Action Creators
export const searchRequest = () => ({
  type: SEARCH_REQUEST,
  isSuccessSearch: false,
  isFetchSearch: true,
  isErrorSearch: false,
});

export const searchSuccess = (json) => ({
  type: SEARCH_SUCCESS,
  isSuccessSearch: true,
  isFetchSearch: false,
  payload: json,
  statusSearch: stauts.success,
  isErrorSearch: false,
});

export const searchFailure = (message) => ({
  type: SEARCH_FAILURE,
  statusSearch: stauts.failure,
  message,
  isSuccessSearch: false,
  isFetchSearch: false,
  isErrorSearch: true,
});

export const search = (keywords) => (dispatch) => {
  dispatch(searchRequest());

  if (!keywords) {
    return dispatch(searchSuccess({}));
  }

  axios
    .get(`${api.urlBase}search?query=${keywords}`)
    .then((response) => {
      return dispatch(searchSuccess(response.data));
    })
    .catch(() => {
      return dispatch(searchFailure("Erro ao realizar busca"));
    });
};
