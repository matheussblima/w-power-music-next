import { combineReducers } from "redux";
import search from "./search";
import rank from "./rank";
import artist from "./artist";
import lyric from "./lyric";
import slide from "./slide";

export default combineReducers({
  search,
  rank,
  artist,
  lyric,
  slide
});
