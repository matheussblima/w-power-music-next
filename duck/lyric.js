import axios from "axios";
import api from "../config/api";

const stauts = Object.freeze({
  success: "SUCCESS",
  failure: "FAILURE",
});

// Actions
const LYRIC_REQUEST = "wpowermusic/LYRIC_REQUEST";
const LYRIC_SUCCESS = "wpowermusic/LYRIC_SUCCESS";
const LYRIC_FAILURE = "wpowermusic/LYRIC_FAILURE";

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case LYRIC_REQUEST:
      return Object.assign({}, state, {
        isFetchLyric: action.isFetchLyric,
        isSuccessLyric: action.isSuccessLyric,
        isErrorLyric: action.isErrorLyric,
      });
    case LYRIC_SUCCESS:
      return Object.assign({}, state, {
        isFetchLyric: action.isFetchLyric,
        isSuccessLyric: action.isSuccessLyric,
        statusLyric: action.statusLyric,
        lyric: action.payload,
        isErrorLyric: action.isErrorLyric,
      });
    case LYRIC_FAILURE:
      return Object.assign({}, state, {
        isFetchLyric: action.isFetchLyric,
        isSuccessLyric: action.isSuccessLyric,
        statusLyric: action.statusLyric,
        message: action.message,
        isErrorLyric: action.isErrorLyric,
      });
    default:
      return state;
  }
}

// Action Creators
export const lyricRequest = () => ({
  type: LYRIC_REQUEST,
  isSuccessLyric: false,
  isFetchLyric: true,
  isErrorLyric: false,
});

export const lyricSuccess = (json) => ({
  type: LYRIC_SUCCESS,
  isSuccessLyric: true,
  isFetchLyric: false,
  payload: json,
  statusLyric: stauts.success,
  isErrorLyric: false,
});

export const lyricFailure = (message) => ({
  type: LYRIC_FAILURE,
  statusLyric: stauts.failure,
  message,
  isSuccessLyric: false,
  isFetchLyric: false,
  isErrorLyric: true,
});

export const getLyric = (artist, music) => (dispatch) => {
  dispatch(lyricRequest());
  return axios
    .get(`${api.urlBase}lyric?artist=${artist}&music=${music}`)
    .then((response) => {
      return dispatch(lyricSuccess(response.data));
    })
    .catch(() => {
      return dispatch(lyricFailure("Erro ao realizar busca"));
    });
};
