import axios from "axios";
import api from "../config/api";

const stauts = Object.freeze({
  success: "SUCCESS",
  failure: "FAILURE",
});

// Actions
const RANK_REQUEST_ARTIST = "wpowermusic/RANK_REQUEST_ARTIST";
const RANK_SUCCESS_ARTIST = "wpowermusic/RANK_SUCCESS_ARTIST";
const RANK_FAILURE_ARTIST = "wpowermusic/RANK_FAILURE_ARTIST";
const RANK_REQUEST_MUSIC = "wpowermusic/RANK_REQUEST_MUSIC";
const RANK_SUCCESS_MUSIC = "wpowermusic/RANK_SUCCESS_MUSIC";
const RANK_FAILURE_MUSIC = "wpowermusic/RANK_FAILURE_MUSIC";

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case RANK_REQUEST_ARTIST:
      return Object.assign({}, state, {
        isFetchRankArtist: action.isFetchRankArtist,
        isSuccessRankArtist: action.isSuccessRankArtist,
      });
    case RANK_SUCCESS_ARTIST:
      return Object.assign({}, state, {
        isFetchRankArtist: action.isFetchRankArtist,
        isSuccessRankArtist: action.isSuccessRankArtist,
        statusRankArtist: action.statusRankArtist,
        rankArtist: action.payload,
      });
    case RANK_FAILURE_ARTIST:
      return Object.assign({}, state, {
        isFetchRankArtist: action.isFetchRankArtist,
        isSuccessRankArtist: action.isSuccessRankArtist,
        statusRankArtist: action.statusRankArtist,
        message: action.message,
      });
    case RANK_REQUEST_MUSIC:
      return Object.assign({}, state, {
        isFetchRankMusic: action.isFetchRankMusic,
        isSuccessRankMusic: action.isSuccessRankMusic,
      });
    case RANK_SUCCESS_MUSIC:
      return Object.assign({}, state, {
        isFetchRankMusic: action.isFetchRankMusic,
        isSuccessRankMusic: action.isSuccessRankMusic,
        statusRankMusic: action.statusRankMusic,
        rankMusic: action.payload,
      });
    case RANK_FAILURE_MUSIC:
      return Object.assign({}, state, {
        isFetchRankMusic: action.isFetchRankMusic,
        isSuccessRankMusic: action.isSuccessRankMusic,
        statusRankMusic: action.statusRankMusic,
        message: action.message,
      });
    default:
      return state;
  }
}

// Action Creators
export const rankRequestArtist = () => ({
  type: RANK_REQUEST_ARTIST,
  isSuccessRankArtist: false,
  isFetchRankArtist: true,
});

export const rankhSuccessArtist = (json) => ({
  type: RANK_SUCCESS_ARTIST,
  isSuccessRankArtist: true,
  isFetchRankArtist: false,
  payload: json,
  statusRankArtist: stauts.success,
});

export const rankFailureArtist = (message) => ({
  type: RANK_FAILURE_ARTIST,
  statusRankArtist: stauts.failure,
  message,
  isSuccessRankArtist: false,
  isFetchRankArtist: false,
});

export const rankRequestMusic = () => ({
  type: RANK_REQUEST_MUSIC,
  isSuccessRankMusic: false,
  isFetchRankMusic: true,
});

export const rankhSuccessMusic = (json) => ({
  type: RANK_SUCCESS_MUSIC,
  isSuccessRankMusic: true,
  isFetchRankMusic: false,
  payload: json,
  statusRankMusic: stauts.success,
});

export const rankFailureMusic = (message) => ({
  type: RANK_FAILURE_MUSIC,
  statusRankMusic: stauts.failure,
  message,
  isSuccessRankMusic: false,
  isFetchRankMusic: false,
});

export const getRankArtist = () => (dispatch) => {
  dispatch(rankRequestArtist());
  return axios
    .get(`${api.urlBase}rank`)
    .then((response) => {
      return dispatch(rankhSuccessArtist(response.data));
    })
    .catch(() => {
      return dispatch(rankFailureArtist("Error ao buscar rank"));
    });
};

export const getRankMusic = () => (dispatch) => {
  dispatch(rankRequestMusic());
  return axios
    .get(`${api.urlBase}rank?type=mus&limit=10`)
    .then((response) => {
      return dispatch(rankhSuccessMusic(response.data));
    })
    .catch(() => {
      return dispatch(rankFailureMusic("Error ao buscar rank"));
    });
};
