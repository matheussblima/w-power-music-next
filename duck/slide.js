import axios from "axios";
import api from "../config/api";

const stauts = Object.freeze({
  success: "SUCCESS",
  failure: "FAILURE",
});

// Actions
const SLIDE_REQUEST = "wpowermusic/SLIDE_REQUEST";
const SLIDE_SUCCESS = "wpowermusic/SLIDE_SUCCESS";
const SLIDE_FAILURE = "wpowermusic/SLIDE_FAILURE";
const SLIDE_DOWNLOAD_REQUEST = "wpowermusic/SLIDE_DOWNLOAD_REQUEST";
const SLIDE_DOWNLOAD_SUCCESS = "wpowermusic/SLIDE_DOWNLOAD_SUCCESS";
const SLIDE_DOWNLOAD_FAILURE = "wpowermusic/SLIDE_DOWNLOAD_FAILURE";

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SLIDE_REQUEST:
      return Object.assign({}, state, {
        isFetchSlide: action.isFetchSlide,
        isSuccessSlide: action.isSuccessSlide,
        isErrorSlide: action.isErrorSlide,
        slide: null,
      });
    case SLIDE_SUCCESS:
      return Object.assign({}, state, {
        isFetchSlide: action.isFetchSlide,
        isSuccessSlide: action.isSuccessSlide,
        statusSlide: action.statusSlide,
        slide: action.payload,
        isErrorSlide: action.isErrorSlide,
      });
    case SLIDE_FAILURE:
      return Object.assign({}, state, {
        isFetchSlide: action.isFetchSlide,
        isSuccessSlide: action.isSuccessSlide,
        statusSlide: action.statusSlide,
        message: action.message,
        isErrorSlide: action.isErrorSlide,
      });
    case SLIDE_DOWNLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetchSlideDownload: action.isFetchSlideDownload,
        isSuccessSlideDownload: action.isSuccessSlideDownload,
      });
    case SLIDE_DOWNLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetchSlideDownload: action.isFetchSlideDownload,
        isSuccessSlideDownload: action.isSuccessSlideDownload,
        statusSlideDownload: action.statusSlideDownload,
      });
    case SLIDE_DOWNLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetchSlideDownload: action.isFetchSlideDownload,
        isSuccessSlideDownload: action.isSuccessSlideDownload,
        statusSlideDownload: action.statusSlideDownload,
      });
    default:
      return state;
  }
}

// Action Creators
export const slideRequest = () => ({
  type: SLIDE_REQUEST,
  isSuccessSlide: false,
  isFetchSlide: true,
  isErrorSlide: false,
});

export const slideSuccess = (json) => ({
  type: SLIDE_SUCCESS,
  isSuccessSlide: true,
  isFetchSlide: false,
  payload: json,
  statusSlide: stauts.success,
  isErrorSlide: false,
});

export const slideFailure = (message) => ({
  type: SLIDE_FAILURE,
  statusSlide: stauts.failure,
  message,
  isSuccessSlide: false,
  isFetchSlide: false,
  isErrorSlide: true,
});

export const slideDownloadRequest = () => ({
  type: SLIDE_DOWNLOAD_REQUEST,
  isSuccessSlideDownload: false,
  isFetchSlideDownload: true,
});

export const slideDownloadSuccess = () => ({
  type: SLIDE_DOWNLOAD_SUCCESS,
  isSuccessSlideDownload: true,
  isFetchSlideDownload: false,
  statusSlideDownload: stauts.success,
});

export const slideDownloadFailure = (message) => ({
  type: SLIDE_DOWNLOAD_FAILURE,
  statusSlideDownload: stauts.failure,
  message,
  isSuccessSlideDownload: false,
  isFetchSlideDownload: false,
});

export const createSlide = (slide) => (dispatch) => {
  dispatch(slideRequest());

  axios
    .post(`${api.urlBase}create-slide`, {
      ...slide,
    })
    .then((response) => {
      return dispatch(slideSuccess(response.data));
    })
    .catch(() => {
      return dispatch(slideFailure("Erro ao gerar slide"));
    });
};

export const downloadSlide = (fileId) => (dispatch) => {
  dispatch(slideDownloadRequest());

  axios
    .get(`${api.urlBase}download/${fileId}`, {
      responseType: "blob",
    })
    .then((response) => {
      const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = downloadUrl;
      link.setAttribute("download", "slide.pptx");
      document.body.appendChild(link);
      link.click();
      link.remove();

      return dispatch(slideDownloadSuccess());
    })
    .catch(() => {
      return dispatch(slideDownloadFailure("Erro ao efetuar o download"));
    });
};
