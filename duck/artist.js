import axios from "axios";
import api from "../config/api";

const stauts = Object.freeze({
  success: "SUCCESS",
  failure: "FAILURE",
});

// Actions
const ARTIST_REQUEST = "wpowermusic/ARTIST_REQUEST";
const ARTIST_SUCCESS = "wpowermusic/ARTIST_SUCCESS";
const ARTIST_FAILURE = "wpowermusic/ARTIST_FAILURE";
const CLEAR_ARTIST = "wpowermusic/CLEAR_ARTIST";

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case ARTIST_REQUEST:
      return Object.assign({}, state, {
        isFetchArtist: action.isFetchArtist,
        isSuccessArtist: action.isSuccessArtist,
        isErrorArtist: action.isErrorArtist,
        artist: null,
      });
    case ARTIST_SUCCESS:
      return Object.assign({}, state, {
        isFetchArtist: action.isFetchArtist,
        isSuccessArtist: action.isSuccessArtist,
        statusArtist: action.statusArtist,
        artist: action.payload,
        isErrorArtist: action.isErrorArtist,
      });
    case ARTIST_FAILURE:
      return Object.assign({}, state, {
        isFetchArtist: action.isFetchArtist,
        isSuccessArtist: action.isSuccessArtist,
        statusArtist: action.statusArtist,
        message: action.message,
        isErrorArtist: action.isErrorArtist,
      });
    case CLEAR_ARTIST:
      return Object.assign({}, state, {
        artist: action.payload,
      });
    default:
      return state;
  }
}

// Action Creators
export const artistRequest = () => ({
  type: ARTIST_REQUEST,
  isSuccessArtist: false,
  isFetchArtist: true,
  isErrorArtist: false,
});

export const artistSuccess = (json) => ({
  type: ARTIST_SUCCESS,
  isSuccessArtist: true,
  isFetchArtist: false,
  payload: json,
  statusArtist: stauts.success,
  isErrorArtist: false,
});

export const artistFailure = (message) => ({
  type: ARTIST_FAILURE,
  statusArtist: stauts.failure,
  message,
  isSuccessArtist: false,
  isFetchArtist: false,
  isErrorArtist: true,
});

export const clearArtistRequest = () => ({
  type: CLEAR_ARTIST,
  payload: {},
});

export const getArtist = (artist) => (dispatch) => {
  dispatch(artistRequest());

  return axios
    .get(`${api.urlBase}artist/${artist}`)
    .then((response) => {
      return dispatch(artistSuccess(response.data));
    })
    .catch(() => {
      return dispatch(artistFailure("Erro ao realizar busca"));
    });
};

export const clearArtist = () => (dispatch) => {
  dispatch(clearArtistRequest());
};
